from django.urls import path

from . import views


app_name = 'assessment'
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('inquiry/', views.InquiryView.as_view(), name="inquiry"),
    path('cardset-list/', views.CardSetListView.as_view(), name="cardset_list"),
    path('card-list/<title>/', views.CardListView.as_view(), name="card_list"),
    path('cardcomment-list/<cardName>/', views.CardCommentListView.as_view(), name="cardcomment_list"),
    path('cardcomment-detail/<int:pk>/', views.CardCommentDetailView.as_view(), name="cardcomment_detail"),
    path('cardcomment-create/', views.CardCommentCreateView.as_view(), name="cardcomment_create"),
    path('cardcomment-update/<int:pk>/', views.CardCommentUpdateView.as_view(), name="cardcomment_update"),
    path('cardcomment-delete/<int:pk>/', views.CardCommentDeleteView.as_view(), name="cardcomment_delete"),
]
