from django.contrib import admin

from .models import CardSet
from .models import Card
from .models import CardComment


admin.site.register(CardSet)
admin.site.register(Card)
admin.site.register(CardComment)
