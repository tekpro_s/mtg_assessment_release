import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from .forms import InquiryForm
from .forms import CardCommentCreateForm
from .models import CardSet
from .models import Card
from .models import CardComment

logger = logging.getLogger(__name__)


class IndexView(generic.TemplateView):
    model = CardComment
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        cardcomment_list = CardComment.objects.all().order_by('-updated_at')
        cardcomment_dict = {'cardcomment_list': cardcomment_list}
        context = cardcomment_dict
        return context

class InquiryView(generic.FormView):
    template_name = "inquiry.html"
    form_class = InquiryForm
    success_url = reverse_lazy('assessment:inquiry')

    def form_valid(self, form):
        form.send_email()
        messages.success(self.request, 'メッセージを送信しました。')
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)


class CardSetListView(generic.ListView):
    model = CardSet
    template_name = 'cardset_list.html'
    paginate_by = 5
    slug_field = "title"
    slug_url_kwarg = "title"

class CardListView(generic.ListView):
    model = Card
    template_name = 'card_list.html'
    paginate_by = 10
    slug_field = "cardName"
    slug_url_kwarg = "cardName"

class CardCommentListView(generic.ListView):
    model = CardComment
    template_name = 'cardcomment_list.html'
    paginate_by = 10
    slug_field = "cardName"
    slug_url_kwarg = "cardName"

    def get_queryset(self):
        return super().get_queryset().order_by('-updated_at')

class CardCommentDetailView(generic.DetailView):
    model = CardComment
    template_name = 'cardcomment_detail.html'

class CardCommentCreateView(LoginRequiredMixin, generic.CreateView):
    model = CardComment
    template_name = 'cardcomment_create.html'
    form_class = CardCommentCreateForm
    success_url = reverse_lazy('assessment:cardset_list')

    def form_valid(self, form):
        cardcomment = form.save(commit=False)
        cardcomment.user = str(self.request.user)
        cardcomment.save()
        messages.success(self.request, 'コメントを投稿しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "コメントの投稿に失敗しました。")
        return super().form_invalid(form)

class CardCommentUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = CardComment
    template_name = 'cardcomment_update.html'
    form_class = CardCommentCreateForm
    success_url = reverse_lazy('assessment:cardset_list')

    def form_valid(self, form):
        messages.success(self.request, 'コメントを更新しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "コメントの更新に失敗しました。")
        return super().form_invalid(form)

class CardCommentDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = CardComment
    template_name = 'cardcomment_delete.html'
    success_url = reverse_lazy('assessment:cardset_list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, "コメントを削除しました。")
        return super().delete(request, *args, **kwargs)