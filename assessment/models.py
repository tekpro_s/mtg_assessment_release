from accounts.models import CustomUser
from django.db import models


class CardSet(models.Model):
    """カードセットモデル"""

    title = models.CharField(verbose_name='カードセット英字', max_length=100)
    cardSetName = models.CharField(verbose_name='カードセット名', max_length=100)

    class Meta:
        verbose_name_plural = 'CardSet'

    def __str__(self):
        return self.title

class Card(models.Model):
    """カードモデル"""

    cardSetTitle = models.ForeignKey(CardSet, verbose_name='カードセット英字', max_length=100, on_delete=models.PROTECT)
    cardName = models.CharField(verbose_name='カード名', max_length=100)

    class Meta:
        verbose_name_plural = 'Card'

    def __str__(self):
        return self.cardName

MARK = (
    ("◎", "◎"),
    ("○", "○"),
    ("△", "△"),
    ("×", "×"),
)

FORMAT = (
    ("スタンダード", "スタンダード"),
    ("パイオニア", "パイオニア"),
    ("モダン", "モダン"),
    ("レガシー", "レガシー"),
    ("ヴィンテージ", "ヴィンテージ"),
    ("パウパー", "パウパー"),
    ("ヒストリック", "ヒストリック"),
    ("統率者戦", "統率者戦"),
    ("シールド", "シールド"),
    ("ドラフト", "ドラフト"),
)

class CardComment(models.Model):
    """カードコメントモデル"""

    cardName = models.ForeignKey(Card, verbose_name='カード名', max_length=100, on_delete=models.PROTECT)
    mark = models.CharField(verbose_name='評価マーク', max_length=100, choices=MARK)
    format = models.CharField(verbose_name='フォーマット', max_length=100, choices=FORMAT)
    comment = models.TextField(verbose_name='コメント本文', max_length=100000)
    user = models.CharField(verbose_name='ユーザー', max_length=100000)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)

    class Meta:
        verbose_name_plural = 'CardComment'

    def __str__(self):
        return self.comment
